// 1. 


let singleDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"]
let tens = ["", "Ten", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"]
let teens = ["", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"]
let hundreds = [" Hundred"]

function numConverter(inputNo)
	{
		if(inputNo < 10 && inputNo >= 0){
			return singleDigit[inputNo]
		}
		if (inputNo < 99 && inputNo%10 === 0) {
			return tens[inputNo/10]
		}
		if (inputNo < 20 && inputNo> 10) {
			return teens[inputNo-10]
		}
		if(inputNo>20 && inputNo<100){
			let singleValue = inputNo%10
			let tensValue = (inputNo-singleValue)/10
			return tens[tensValue] + " " + singleDigit[singleValue]
		}
		if (inputNo <999 && inputNo%10===0){
			let tensHundred = (inputNo%100)/10
			let Hundred = (inputNo-(tensHundred*10))/100
			return singleDigit[Hundred] + " " + hundreds + " " + tens[tensHundred]
		}
		if (inputNo <910 && inputNo %10>=1) {
			let singleHundred = (inputNo%10)
			let tensHundred = (inputNo%100)/10
			let Hundred = (inputNo-(tensHundred*10))/100
			return singleDigit[Hundred] + " " + hundreds + " " + singleDigit[singleHundred]
		}
	}