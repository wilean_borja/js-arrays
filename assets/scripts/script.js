// Arrays
// used to store multiple values in a single variable

//  array = []

let students = ["Darwin", "Rancel", "Yong", "Kaka"];
let randomThings = [23, "Albert", true];
// console.log(randomThings);
// console.log(students[0]);

// index - location of data: Darwin = 0; Rancel = 1; Yong = 3; Kaka = 4;

// .length - to know how many elements are there inside the array
// console.log(students[0].length);

// let password = "ihateyou";
// console.log(password.length);
// if (password.length < 9) {
// 	console.log("Invalid Password");
// }

// add a value to an array
// .push("value to be added")
// students.push("Krystel");
// console.log(students);	

// To remove the last value in an array and return the new array
// .pop("value to be removed, removes latest value"). Returns new element after removal

// console.log(students.pop());
// console.log(students);

// to remove an element based on its index;
// .splice(index, # of element we want to remove, element we want to add)

// console.log(students.splice(1, 2));
// console.log(students);

// add Fern, remove Rancel
// console.log(students.splice(1, 1, "Fern"));

// No removal, just add Fern
// console.log(students.splice(2, 0, "Fern"));
// console.log(students);


// Iteration Method
// .forEach - for each element in the array do this
// student is not defined/declared, but function is anonymous. forEach automatically assigns the data from 'students' to a variable. function 'student' can be any name.

students.forEach(function(student, index){
	if(index === 0 || index ===3){
	console.log("Constestant #" + (index+1) + "; " + student + " Pogi");	
	}
	else{
		console.log("Thank you for joining " + student);
	}
})
